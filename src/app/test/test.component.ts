import { Component, OnInit, Input, Output ,EventEmitter} from '@angular/core';


@Component({

  // -------------------------------class binding and style binding---------------------------------
//   selector: 'app-test',
//   template: `<div>welcome</div>
  
//   <input type="text" value="code" id={{myid}}>
//   <input type="text" value="codeR" [id]="myid" [disabled]="isdisabled">

//   <h3 class="danger">Component</h3>
//   <h3 [class]="status">Component</h3>
//   <h3 [class.danger]="condition">Component</h3>
//   <h3 [ngClass]="combo">Component</h3>
  
//   <h3 [style.color]="'brown'">Style-Binding</h3>
//   <h3 [style.color]="condition ? 'red' : 'green' ">Style-Binding</h3>
//   <h3 [style.color]="color">Style-Binding</h3>
//   <h3 [ngStyle]="paint">Style-Binding</h3>


//   `
  
// ,
//   styles: [`
//   .danger{
//     color:red;
//   }
//   .success{
//     color:green;
//   }
//   .special{
//     font-style:italic;
//   }
//   `]
  // -------------------------------class binding and style binding---------------------------------


  // -------------------------------event binding---------------------------------

//   selector:'app-test',
//   template:`<h2>welcome</h2>
  
//   <button (click)="press($event)">click </button><br>
//   <button (click)="greet='Welcome RAHUL'">press </button>

//  <h5> {{greet}}</h5>
//  <h5>{{magic}}</h5>
//   `,
//   styles:[``]

  // -------------------------------event binding---------------------------------


  // -------------------------------Template reference variable---------------------------------

//  selector:'app-test',
//  template:`
//  <h3>WELCOME</h3>
//  <input type="text" #id><br>
//  <button (click)="click(id)">press </button>
//  <button (click)="clicks(id.value)">press to watch value </button>


//  `,
//  styles:[]
  // -------------------------------Template reference variable---------------------------------

  // -------------------------------Two way binding---------------------------------

// selector:'app-test',
// template:`
//            <h3>welcome </h3>

//            <input type="text" [(ngModel)]="myname">
//            {{myname}}


// `,
// styles:[]

  // -------------------------------Two way binding---------------------------------


  // -------------------------------ng-if---------------------------------


//   selector: 'app-test',
//   template:`
//   <h1 *ngIf="false; else elsepart"> TATA CONSULTANCY SERVICES</h1>
  
//   <ng-template  #elsepart>
//   <h3>Some values are hidden</h3>
//   </ng-template>


// <div *ngIf="decision ; then ifname; else elsename" ></div>
// <ng-template #ifname>IF- PART</ng-template>
// <ng-template #elsename>ELSE- PART</ng-template>


  
//   `,
//   styles:[]



 // -------------------------------ng-if---------------------------------

// -------------------------------ng-switch--------------------------------
// selector:'app-test',
// template:`

// <div [ngSwitch]="color">

// <div *ngSwitchCase="'red'">You pick red color.</div>
// <div *ngSwitchCase="'pink'">You pick pink color.</div>
// <div *ngSwitchCase="'green'">You pick green color.</div>
// <div *ngSwitchDefault>Color dont exist</div>

// </div>

// `,
// styles:[]

 // -------------------------------ng-switch--------------------------------

 // -------------------------------ng-for and component interaction--------------------------------

//  selector:'app-test',
//  template:`
 
//  <div *ngFor="let color of colors; index as i;first as f; even as e">
//  <h3>{{i}} {{f}} {{color}} {{e}}</h3>
//  </div>


//  <h1>{{"I WORK FOR "+ parentData}}</h1>
//  <button (click)="fire()">Event</button>

 
//  `,
//  styles:[]




 // -------------------------------ng-for and component interaction--------------------------------
 

 // -------------------------------pipes-------------------------------

 selector:'app-test',
 template:`
 
 <h2>{{rule}}</h2>
 <h2>{{rule | uppercase}}</h2>
 <h2>{{rule | slice:4:6}}</h2>
 <h2>{{person| json}}</h2>
 <h2>{{22.47 | number:'2.2-3'}}</h2>
 <h2>{{22.47 | number:'3.3-4'}}</h2>
 <h2>{{22.47 | number:'7.7-8'}}</h2>
 <h2>{{0.32 | percent}}</h2>
 <h2>{{0.32 | currency}}</h2>

 <h2>{{0.32 | currency : 'EUR'}}</h2>

 <h2>{{date }}</h2>
 <h2>{{date | date:'short'}}</h2>
 <h2>{{date |date:'shortTime'}}</h2>
 <h2>{{date |date: 'shortDate'}}</h2>





 `,
 styles:[]
 // -------------------------------pipes--------------------------------


})
export class TestComponent implements OnInit {

  public date= new Date();
public isdisabled=true;
  public myid="testid";
  public name="RAHUL";
  public rule="knowledge";

  public status="success";
  public condition=false;

  public combo={
    "danger" : !this.condition,
    "success": this.condition,
    "special":true
  };

  public color="pink";
  public greet="";
  public magic="";
  public myname="";

  public paint={
    color: "orange",
    fontStyle:"italic",

  }

  public decision=false;
  public colors=["red","green","yellow","blue"];

  public person={

    "firstname":"Renu",
    "lastname":"Manral",

  };

   @Input() public parentData;
   @Output() public child= new EventEmitter();

  
  constructor() { }

  ngOnInit(): void {
  }


  press(event){
    console.log(event);
    this.greet=event.screenX;
    this.magic="I appeared on click of this button";
  }

  click(id){
    console.log(id);
  }

  clicks(value){
    console.log(value);
  }

  fire(){

    this.child.emit("HELLO parent,I send data from child component");

  }

}
